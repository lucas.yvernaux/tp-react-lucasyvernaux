import { Component } from 'react';
import { Button } from 'react-bootstrap';

class Example3 extends Component {

    constructor(){
        super();
        this.state = {
            isToggle: true
        }
    }

    handleClick(){
        this.setState({
            isToggle: !this.state.isToggle
        })
    }

    render() {
        return (
            <div>
                <h2>Le Toggle !</h2>
                <Button variant={this.state.isToggle ? 'primary' : 'secondary'} onClick={() => this.handleClick()}>{this.state.isToggle ? 'ON' : 'NON'}</Button>
                <p>switch {this.state.isToggle.toString()}</p>
            </div>
        );
    }
}
export default Example3;