import { Component } from 'react';
import { Button, Form } from 'react-bootstrap';

class Example4 extends Component {

    constructor(){
        super();
        this.state = {
            userName: ''
        }
    }

    handleChange(event){
        this.setState({
            userName: event.target.value
        })
    }

    handleSubmit(event){
        event.preventDefault();
        alert('bonjour '+this.state.userName);
    }

    render() {
        return (
            <div>
                <h2>Formulaire </h2>
                <Form onSubmit={(event) => this.handleSubmit(event)}>
                    <Form.Label>
                        <h3>Votre nom</h3>
                    </Form.Label>
                    <Form.Control type='text' onChange={(event) => this.handleChange(event)}></Form.Control>
                    <Button type='submit'>Submit</Button>
                </Form>
            </div>
        );
    }
}
export default Example4;