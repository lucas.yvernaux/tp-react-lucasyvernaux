import { useEffect, useState } from 'react';

function ExampleApi() {

    const [items,setItems] = useState([]);

    useEffect(() => {
        fetch("https://jsonplaceholder.typecode.com/users")
        .then((res) => res.json())
        .then((json) => {
            setItems({
                items: json
            });
        })
        .catch(err => console.error("ERROR :"+err))
    }, []);
    
    return (
        <div className='container'>
            <h5>Api Example</h5>
            {items.map((item) => (
                <p>{items.username}</p>
            ))}
        </div>
    );
}
export default ExampleApi;