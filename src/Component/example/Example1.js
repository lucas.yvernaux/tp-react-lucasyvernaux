import { Component } from 'react';
import { Button } from 'react-bootstrap';

class Exemple1 extends Component {

    constructor(){
        super();
        this.state = {
            compteur: 3
        }
    }

    increment() {
        this.setState({
            compteur: this.state.compteur + 1
        })
    }
    reset() {
        this.setState({
            compteur: 0
        })
    }
    render() {
        return (
            <div>
                <h2>Compteur</h2>
                <p>{ this.state.compteur}</p>
                <Button variant='primary' onClick={() => this.increment() }>Cliquer</Button>
                <Button variant='secondary' onClick={() => this.reset() }>Reset</Button>
            </div>
        );
    }
}
export default Exemple1;