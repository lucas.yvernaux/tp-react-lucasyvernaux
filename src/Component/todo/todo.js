//  Import des fonctions react.
import { useState } from 'react';

//  Import des Composants créer.
import TodoForm from './todoForm';
import TodoList from './todoList';

// Composant Todo
function Todo() {

    const [todo, setTodo] = useState("");
    const [todoList, setTodoList] = useState([]);

    
    return (
        <div className='container'>     
            <TodoForm todo={todo}
                setTodo={setTodo}
                todoList={todoList}
                setTodoList={setTodoList}
            />
            <TodoList todoList={todoList} setTodoList={setTodoList}/>
        </div>
    );
}
export default Todo;