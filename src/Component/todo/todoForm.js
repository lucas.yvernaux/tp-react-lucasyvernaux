//  Import des composants bootstrap
import { Button, Form } from 'react-bootstrap';

//  Import de la librairie Nanoid
import { nanoid } from 'nanoid';

//Composants TodoForm représentant le formulaire et sa logique de la Todo.
function TodoForm({todo, setTodo, todoList, setTodoList}) {
    
    const handleChange = (event) => {
        setTodo(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault();   
        setTodoList([...todoList, {name:todo, id:nanoid()}]);
        setTodo("");
    }

    return (
        <div>
            <h2>Add Todo</h2>
            <Form onSubmit={handleSubmit}>
                <Form.Group className='mb-3 d-grid gap-2'>
                    <Form.Control type='text' value={todo} onChange={handleChange}></Form.Control>
                    <Button size='lg' type='submit'>Add</Button>
                </Form.Group>
            </Form>
        </div>
    );
};
export default TodoForm;