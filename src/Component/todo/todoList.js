//  Import des composants bootstrap
import Button from 'react-bootstrap/Button';

//  Import des composants créer
import TodoListItem from "./todoListItem";

//Composant TodoList représentant la logique et le contenant de la liste Todo
function TodoList({todoList, setTodoList}) {
    
    //Fonction vidant entièrement la Todo liste.
    const clearList = () => {
        setTodoList([]);
    }

    return (
        <div className="container">
            Todo size : {todoList.length}
            {todoList.map((listItem) => (
                <TodoListItem todoList={todoList}
                    setTodoList={setTodoList}
                    listItem={listItem}
                    key={listItem.id}
                    name={listItem.name}/>
            ))}
            <Button className='float-end' onClick={clearList} variant='danger' disabled={todoList.length == 0}>Clear</Button>
        </div>
    );
};
export default TodoList;