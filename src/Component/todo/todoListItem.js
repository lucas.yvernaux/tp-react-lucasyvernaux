//  Import des composants bootstrap
import Button from 'react-bootstrap/Button';

const itemNameStyle = {
    textAlign: 'center',
    padding: '0.7em',
    marginTop: '10px;'
}

//Composant TodoListItem representant un élément de la liste et sa logique Todo
function TodoListItem({listItem, todoList, setTodoList}) {
    
    const deleteItem = () => {
        setTodoList(todoList.filter(item => item.id != listItem.id));
    }

    return (
       <div className="container itemStyle">
        {/* Voir dans le GlobalStyle pour la classe itemStyle */}
            <h5 style={itemNameStyle}>{listItem.name}</h5>
            <div className='col-md-12 text-center'>
                <Button onClick={deleteItem} variant='danger'>Delete</Button>
            </div>
        </div>
    );
}
export default TodoListItem;