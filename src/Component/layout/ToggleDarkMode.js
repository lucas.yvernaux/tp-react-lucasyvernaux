// Import des composants Material-UI
import { Brightness4 } from '@material-ui/icons';
import { Brightness7 } from '@material-ui/icons';
import { IconButton } from '@material-ui/core';
import { Box } from '@material-ui/core';

const boxStyle = {
    cursor: 'pointer'
}

//Composant switch pour changer de mode dark/light .
function ToggleDarkMode ({theme,  toggleTheme }) {


    return (
        <Box onClick={toggleTheme} style={boxStyle}>
            mode {theme}
            <IconButton color="inherit">
                {theme === 'dark' ? <Brightness7 /> : <Brightness4 />}
            </IconButton>
        </Box>
                
    );
};
export default ToggleDarkMode;
