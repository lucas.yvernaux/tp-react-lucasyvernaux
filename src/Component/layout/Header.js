//  Import des composants bootstrap
import Navbar from 'react-bootstrap/Navbar';

//  Import des composants créer
import ToggleDarkMode from './ToggleDarkMode';

//Composant represantant le header de la page
function Header({theme,  toggleTheme }) {

    return (
        <Navbar bg="primary">
            <Navbar.Brand>
                Cours React Lucas
            </Navbar.Brand>
            <Navbar.Collapse className="justify-content-end">
                <ToggleDarkMode theme={theme} toggleTheme={toggleTheme} />
            </Navbar.Collapse>
        </Navbar>        
    )
};
export default Header;
