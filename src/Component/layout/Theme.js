// css appelée lorsque que le mode light est activée.

export const lightTheme = {
    body: '#FFF',
    text: '#363537',
    toggleBorder: '#FFF',
    itemBackground: 'grey',
    navBorder: 'grey',
    background: '#363537',
}

// css appelée lorsque que le mode dark est activée.

export const darkTheme = {
    body: '#363537',
    text: '#FAFAFA',
    toggleBorder: '#6B8096',
    itemBackground: 'green',
    navBorder: 'whitesmoke',
    background: '#999',
}
