import './App.css';

//  Import des fonctions React
import { useState } from 'react';

//  Import des librairies
import {ThemeProvider} from "styled-components";
import { GlobalStyles } from "./Component/layout/globalStyle";
import { lightTheme, darkTheme } from "./Component/layout/Theme"

//  Import des composants créer
/*import Example1 from  './Component/example/Example1';
import Example2 from  './Component/example/Example2';
import Example3 from  './Component/example/Example3';
import Example4 from  './Component/example/Example4';
import Todo from  './Component/todo/todo';
import ExampleApi from './Component/example/ExampleApi';*/
import Todo from './Component/todo/todo';
import Header from './Component/layout/Header';

function App() {

  const [theme, setTheme] = useState('light');
  const themeToggler = () => {
    theme === 'light' ? setTheme('dark') : setTheme('light')
  }

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <GlobalStyles/>
      <Header theme={theme} toggleTheme={themeToggler} />
      <div className='container'>         
        <Todo/>
      </div>
    </ThemeProvider>
  );
}

export default App;
